package com.example.cartesviasual;

import com.example.cartesviasual.Card;

import java.util.Random;

public class Deck {

    private Card[] cards;

    public Deck() {
        int idx = 0;
        cards = new Card[Card.getNbColors() * Card.getNbValues()];

        for (String color : Card.getColors()) {
            for (String value : Card.getValues()) {
                cards[idx++] = new Card(color, value);
            }
        }
    }

    public int getNbCards() {
        return cards.length;
    }

    /**
     * Fisher & Yates shuffle (https://en.wikipedia.org/wiki/Fisher-Yates_shuffle)
     *
     */
    public void shuffle() {
        int idx;
        Card tmp;
        Random rndGen = new Random();

        for (int i = cards.length - 1; i > 0; i--) {
            idx = rndGen.nextInt(i+1);
            tmp = cards[idx];
            cards[idx] = cards[i];
            cards[i] = tmp;
        }
    }

    public Card[] draw(int nbCards) {
        if (cards.length == 0) {
            System.err.println("No ch.hevs.oop.challenges.cards left in the deck");
            nbCards = 0;
        }
        else if ((nbCards <= 0) || (nbCards > cards.length)) {
            System.err.println("Invalid number of ch.hevs.oop.challenges.cards. Let's return a single card.");
            nbCards = 1;
        }

        // Draw nbCards
        Card[] rv = new Card[nbCards];

        for (int i = 0; i < nbCards; i++) {
            rv[i] = cards[i];
        }

        // Update your deck
        Card[] newDeck = new Card[cards.length-nbCards];
        for (int i=0; i<cards.length-nbCards; i++) {
            newDeck[i] = cards[i+nbCards];
        }
        cards = newDeck;

        return rv;
    }

    @Override
    public String toString() {
        String rv = "";

        for (int i = 0; i < cards.length; i++) {
            rv += cards[i];
            if (i < cards.length-1) {
                rv += " - ";
            }
        }

        return rv;
    }
}
