package com.example.cartesviasual;

public class Card {

    private static final String[] colors = { "spades", "heart", "diamond", "club"};
    private static final String[] values = { "ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "jack", "queen", "king"};

    private String color;
    private String value;

    public Card() {
        this(colors[0], values[0]);
    }

    public Card(String color, String value) {
        // Do we have valid inputs?
        if (!contains(colors, color) || !contains(values, value)) {
            this.color = colors[0];
            this.value = values[0];
            System.out.println("Unknown card: " + value + " of " + color);
            System.out.println("Setting a default value: " + value + " of " + color);
        }
        this.color = color;
        this.value = value;
    }

    private boolean contains(String[] data, String value) {
        boolean rv = false;
        for (String item: data) {
            if (item.equals(value.toLowerCase())) {
                rv = true;
                break;
            }
        }
        return rv;
    }

    public static String[] getColors() {
        return colors;
    }

    public static int getNbColors() {
        return colors.length;
    }

    public static String[] getValues() {
        return values;
    }

    public static int getNbValues() {
        return values.length;
    }

    @Override
    public String toString() {
        return this.value + " of " + this.color;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Card && color.equals(((Card)o).color) && value.equals(((Card)o).value);
    }
}
