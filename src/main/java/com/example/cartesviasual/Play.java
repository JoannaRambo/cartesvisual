package com.example.cartesviasual;

import com.example.cartesviasual.Card;
import com.example.cartesviasual.Deck;

public class Play {

    public static void main(String[] args) {
        Deck myDeck = new Deck();

        System.out.println("New deck:\n" + myDeck);
        System.out.println("Let's shuffle it!");
        myDeck.shuffle();
        System.out.println(myDeck);
        System.out.print("Draw 5 ch.hevs.oop.challenges.cards:\t");
        for (Card card: myDeck.draw(5)) {
            System.out.print(card + "\t");
        }
        System.out.println();

    }
}
